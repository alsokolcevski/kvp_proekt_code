#include <PID_v1.h>

#include <Wire.h> // biblioteka za I2C komunikacija
#include <LiquidCrystal_I2C.h> // biblioteka za lCD display so I2C

LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2); 

//pinot povrzan so releto
#define RELAY_PIN 6

int ThermistorPin = A3;
int Tc;
int Vo;
float R1 = 10000;
float logR2, R2, T;
float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;
int Tc_fan;
int PWMVal;



double Setpoint, Input, Output;

//Parametrite
double Kp=280, Ki=38, Kd=1.4;
PID myPID(&Input, &Output, &Setpoint, Kp, Ki, Kd, DIRECT);

int WindowSize = 10000;
unsigned long windowStartTime;




void setup()
{
  
  pinMode(RELAY_PIN, OUTPUT);
  digitalWrite(RELAY_PIN, LOW);
  Serial.begin(115200);
  Serial.println("Starting");
  windowStartTime = millis();

  //posakuvanata vrednost
  Setpoint = 35;

  //tell the PID to range between 0 and the full window size
  myPID.SetOutputLimits(0, WindowSize);

  //turn the PID on
  myPID.SetMode(AUTOMATIC);

  lcd.init();
  lcd.backlight();
}

void loop()
{
  analogWrite(9,255);
  Vo = analogRead(ThermistorPin);
  R2 = R1 * (1023.0 / (float)Vo - 1.0);
  logR2 = log(R2);
  T = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
  Tc = T - 273.15;
//  Tf = (Tc * 9.0)/ 5.0 + 32.0;

  Serial.print("Temperatura: ");
  Serial.println(Tc);
  delay(500);

  

  lcd.setCursor(0, 0); //prva kolona , prv red na LCD display
  lcd.print("Temperatura="); 
  lcd.setCursor(12,0); 
  lcd.print(Tc);
  lcd.setCursor(14,0);
  lcd.print("C");
  Serial.print("Temperatura: ");
  Serial.println(Tc);
  
  
  

  Tc_fan = Tc;
  if(Tc>40)
  {
    Tc_fan= 40;
  }

  PWMVal = map(Tc_fan, 15, 30, 26, 255);

  if(Tc_fan<20)
  {
    PWMVal = 0;
  }

  analogWrite(9, PWMVal);

  Input = Tc;
  Serial.print("Temperatura: ");
  Serial.println(Input);
  myPID.Compute();

  /************************************************
   * on/off na releto vo zavisnost od pid izlezot
   ************************************************/
  if (millis() - windowStartTime > WindowSize)
  {
    //time to shift the Relay Window
    windowStartTime += WindowSize;
  }
  if (Output < millis() - windowStartTime) 
    digitalWrite(RELAY_PIN, HIGH);
  else 
    digitalWrite(RELAY_PIN, LOW);

}
