#include <Wire.h> // biblioteka za komunikacija
#include <LiquidCrystal_I2C.h> //biblioteka za LCD display so I2C
LiquidCrystal_I2C lcd = LiquidCrystal_I2C(0x27, 16, 2); 

int rele = 6;
int ThermistorPin = A3;
int Tc;
int Vo;
float R1 = 10000;
float logR2, R2, T;
float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;
int Tc_fan;
int PWMVal;


void setup() {
Serial.begin(115200);
pinMode(rele,OUTPUT);
digitalWrite(rele,HIGH);
lcd.init();
lcd.backlight();
}

void loop() {

  analogWrite(9,255);
  Vo = analogRead(ThermistorPin);
  R2 = R1 * (1023.0 / (float)Vo - 1.0);
  logR2 = log(R2);
  T = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
  Tc = T - 273.15;


  lcd.setCursor(0, 0); //prva kolona , prv red na LCD display
  lcd.print("Temperatura=");
  lcd.setCursor(12,0);
  lcd.print(Tc);
  lcd.setCursor(14,0);
  lcd.print("C");
  Serial.print("Temperatura: ");
  Serial.println(Tc);
  
              
  

  Tc_fan = Tc;
  if(Tc>25)
  {
    Tc_fan= 25;
  }

  PWMVal = map(Tc_fan, 15, 25, 26, 255);

  if(Tc_fan<15)
  {
    PWMVal = 0;
  }

  analogWrite(9, PWMVal);




  if(Tc<35)
    {
      digitalWrite(rele,LOW);
      delay(500);
  }
  else
   {
     digitalWrite(rele,HIGH);
     delay(500);
  }
}
